Cat Escape: Meow or Never is an action puzzle catformer that allows the player to feel the joy of being a cat. The player controls an orange tabby cat as it tries to escape from a hoarder’s mansion, climbing up fuzzy blocks, eating mice, and shooting hairballs to defeat enemies along the way. The house is falling apart and infested with vermin. An evil vacuum even guards the way out. Can you help our hero escape?

This game was made by Those Who Wander consisting of Derek Donahue, Joe Rossi, Brittany West, and Joe Wise.

Play Cat Escape: Meow or Never
http://www.joethered.com/cat-escape/

Controls:

Left/Right: Move

Up/Down (while attached to a fuzzy wall): Climb

X: Jump

Z (while fat): Shoot Hairball